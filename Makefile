#################################################################### OVERVIEW
#
#  Makefile for the AstroGK Gyrokinetic Turbulence code 
#  (requires GNU's gmake)
#
#  Makefile written by Bill Dorland
#
#  LAST UPDATE: 12/10/06
#
######################################################################## TODO
#
#  First do "module load netcdf " (at NERSC) or otherwise set up your
#  compilation/library environment.
#
#  Then type:
#
#    "make" to build an optimized version of AstroGK
#    "make debug=on" to build debug version
#    "make static=on" to build semi-portable version
#
# This uses the shell script ./test_os to figure out which platform 
# we are running on to set the right compiler switches and library 
# paths. Or type "make CPU=C90", or "setenv CPU C90" to compile for 
# Cray C90/J90. 
#
# Alternatively, the "debug" and/or "CPU" make options can be set as 
# environment variables, and then run make.  For example:
#
#    setenv CPU T3E ; setenv debug on ; make
#
# Or set defaults by uncommenting one of the following lines:
# 
# CPU = T3E
# debug  = on
#
# NEW FEATURES:
#
#   "make test_make" - print values of variables
#   "make distclean" - does "make clean" + removes platform links & executables
# 
#
#
# CPU OPTIONS: 
#
# LINUX    	(for MIT linux cluster with NAG f95)
# LINUX_fuj 	(for linux with fujitsu f90)
# LINUX_alpha 	(for linux with Compaq Alpha compiler)
# RS6000	(for IBM SP2 with xlf)
# ALPHA		(for DEC ALPHA with DEC f95)
# ALPHA_NAG	(for DEC ALPHA with NAG f95)
#
####################################################################### CPU
#
# environment variable CPU is used to set platform-dependent make variables.
#
# If CPU is undefined; use the test_os script to figure it out:

ifeq ($(CPU),)
   CPU_tmp := $(shell sh ./test_os)
   ifeq ($(CPU_tmp),ALPHA)
      CPU := ALPHA
#     CPU := ALPHA_NAG
   else
      CPU := $(CPU_tmp)
   endif
endif

DATE=$(shell date +%D | sed 's/\///g')

##################################################### PLATFORM DEPENDENT FLAGS

# (in gnu make, $$ translates to $, and shell then does variable substitution)

# T3E options:
ifeq ($(CPU),T3E)
  FC = f90
  PLATFORM_LINKS = t3e
  F90FLAGS = -I$$MPTDIR/include -M1110,7212
  FLIBS = -lmpi $$NETCDF -Wl"-D permok=yes"
ifeq ($(FFT),fftw)
  FLIBS += -L../fftw/lib -lfftw -lrfftw
endif
  ifneq ($(debug),on)
    F90FLAGS += -O vector3 -O aggress 
  else
    F90FLAGS += -g -R abcs -e i
    FLIBS  += -Wl"-D preset=inf" # -lmalloc 
  endif

endif

# NERSC IBM options:
ifeq ($(CPU),RS6000)
  FC = mpxlf90_r
  FC5 = h5pfc
  PLATFORM_LINKS = ibm
  F90FLAGS = -qautodbl=dbl4 -qsuffix=f=f90 \
	-I $(NETCDF_DIR)/include 
# PROBLEM Compiling on Bassi 30JAN08 GGH: 
#    I had to change line below to append a 'd' to each library's name
#  FLIBS = $$NETCDF $$FFTW -lfftw -lrfftw
  FLIBS = $$NETCDF $$FFTW -ldfftw -ldrfftw
  FLIBS5 = $$NETCDF $$FFTW -lfftw -lrfftw \
	-I/usr/common/usg/hdf5/64/1.6.4/parallel/lib \
	-L/usr/common/usg/zlib/default/lib -L/usr/common/usg/szip/default/lib \
	/usr/common/usg/hdf5/64/1.6.4/parallel/lib/libhdf5_fortran.a \
	/usr/common/usg/hdf5/64/1.6.4/parallel/lib/libhdf5.a -lsz -lz -lm
  ifneq ($(debug),on)
#    F90FLAGS += -O4
    F90FLAGS += -O3 -qarch=auto -qtune=auto
  else
    F90FLAGS += -g 
    FLIBS    += # $$TRACE_MPIF
  endif

endif

# NERSC Cray XT4 (Franklin) options: GGH 07 SEP 17
ifeq ($(CPU),XT4)
  FC = ftn
  PLATFORM_LINKS = cxt4
  F90FLAGS = -r8 -target=linux -I$$NETCDF_DIR/include $$FFTW_INCLUDE_OPTS
#  F90FLAGS = -default64 -target=linux -I$$NETCDF_DIR/include $$FFTW_INCLUDE_OPTS
  FLIBS = -L$$NETCDF_DIR/lib -lnetcdf $$FFTW_POST_LINK_OPTS -ldfftw -ldrfftw
  ifneq ($(debug),on)
    F90FLAGS +=   -fastsse
  else
    F90FLAGS += -g 
  endif	
endif

# Dawson G5 cluster:
ifeq ($(CPU),Dawson)
  FC = xlf95
  PLATFORM_LINKS = ibm
  F90FLAGS = -qmoddir=/tmp/ghowes -I/tmp/ghowes \
	-qautodbl=dbl4 -qsuffix=f=f90 \
	-I /u/local/apps/netcdf/include -I/u/local/mpi/mpilam/include 
  FLIBS = -L/u/local/apps/netcdf/lib -lnetcdf \
	-L/u/home2/nfl/FFTW/lib -lfftw -lrfftw \
	-L/u/local/mpi/mpilam/lib -llammpio -llamf77mpi -lmpi -llam
#-L/u/local/apps/fftw/lib -lfftw -lrfftw 
  ifneq ($(debug),on)
#    F90FLAGS += -O4
    F90FLAGS += -O3 -qarch=g5 -qtune=g5
  else
    F90FLAGS += -g 
    FLIBS    += # $$TRACE_MPIF
  endif

endif

# options for Linux with Fujitsu f90
ifeq ($(CPU),LINUX_fuj)
  FC = mpif90
  FLIBS = -L/usr/local/lib -lnetcdf \
	-L/u/ghowes/fftw/lib -lfftw -lrfftw
  PLATFORM_LINKS = linux_fuj
  F90FLAGS = -A m -C cdRR8 -I/usr/local/include -X9 \
	-static-flib -Kfast

  ifeq ($(debug),on) 
    F90FLAGS += -g -H easu
  else
    F90FLAGS += -O -f 2004,2006,2008 -Wa,--no-warn
  endif

endif

# options for Linux with Absoft f90
ifeq ($(CPU),LINUX_abs)
  FC = mpif90
  PLATFORM_LINKS = linux_abs

#  FLIBS = -L/usr/local/lib -lrfftw -lfftw 
  FLIBS = -L~kthcmr/fftw -lrfftw -lfftw 
  FLIBS +=  -L/usr/lib -lnetcdf

  F90FLAGS_base = -Rp -I/usr/include -N113 
  F90FLAGS_0    = $(F90FLAGS_base) -YEXT_SFX= 
  F90FLAGS_1    = $(F90FLAGS_base) -YEXT_SFX=_
  F90FLAGS_2    = $(F90FLAGS_base) -YEXT_SFX=__

  F90FLAGS = $(F90FLAGS_base)

  ifeq ($(debug),on) 
    F90FLAGS += -g -Rbcs
    F90FLAGS_0 += -g -Rbcs
    F90FLAGS_1 += -g -Rbcs
    F90FLAGS_2 += -g -Rbcs
  else
    F90FLAGS += -O 
    F90FLAGS_0 += -O 
    F90FLAGS_1 += -O 
    F90FLAGS_2 += -O 
  endif

endif

# options for Linux with Lahey lf95
ifeq ($(CPU),LINUX_lf95)
  FC = mpif90
  FLIBS = -L/usr/local/lib -lnetcdf \
	-L/usr/local/lib -lfftw -lrfftw
  PLATFORM_LINKS = linux_lf95
  F90FLAGS = --dbl --ml cdecl -I/usr/local/include

  ifeq ($(static),on)
    F90FLAGS += --staticlink
  endif

  ifeq ($(debug),on) 
# -mpitrace -mpianim -mpilog
    F90FLAGS += -g --chk aesu 
#    F90FLAGS += -O -mpitrace 
  else
    F90FLAGS += -O 
  endif

endif

# options for Linux with NAG f95:
ifeq ($(CPU),LINUX)
  FC = mpif90
  FLIBS = -L/usr/local/lib -lnetcdf \
	-L/usr/local/lib -lfftw -lrfftw
  PLATFORM_LINKS = linux
  F90FLAGS = -w -r8 -mismatch -I/opt/mpich.gccf95-2/include \
	-I/usr/local/include 
  F90FLAGS_2  = -w -mismatch -I/opt/mpich.gccf95-2/include \
	-I/usr/local/include 
  F90FLAGS_3  = -r8 -mismatch -I/opt/mpich.gccf95-2/include \
	-I/usr/local/include 

  ifeq ($(debug),on)
    F90FLAGS += -C=array -C=bits -C=dangling -C=do -C=present -C=pointer -gline 
    F90FLAGS_2 += -C=array -C=bits -C=dangling -C=do -C=present -C=pointer -gline
    F90FLAGS_3 += -C=array -C=bits -C=dangling -C=do -C=present -C=pointer -gline
  else
    F90FLAGS += -O4 
  endif

endif

########################################################## MODULE DECLARATIONS

# Cray compiler automatically searches for module *.o files in present
# directory, but NAG compiler doesn't, so have to explicitly list them:

GKMOD= constants.o mp.o agk_layouts.o command_line.o agk_save.o \
	text_options.o file_utils.o ran.o redistribute.o antenna_data.o \
	agk_reinit.o agk_time.o fft_work.o antenna.o \
	theta_grid.o kgrids.o dist_fn_arrays.o species.o agk_dist_io.o \
	fields_arrays.o le_grids.o collisions.o agk_transforms.o \
	nonlinear_terms.o \
	fields.o fields_implicit.o init_g.o \
	dist_fn.o hyper.o agk_diagnostics.o agk_io.o netcdf_mod.o \
	run_parameters.o agk_heating.o spl.o 

EGRIDMOD = spl.o constants.o

EXPMOD=	agk_layouts_serial.o constants.o spl.o 

ifeq ($(CPU),LINUX)
  GKMOD += nag_args.o
  mp.o: nag_args.o
endif

LINKS= command_line.f90 mp.f90 redistribute.f90 ran.f90 agk_layouts.f90 \
	agk_save.f90 agk_transforms.f90 fft_work.f90

######################################################################## RULES
.SUFFIXES:
.SUFFIXES: .f90 .o

.f90.o: 
	$(FC) $(F90FLAGS) -c $<

################################################################### DIRECTIVES
# search path where gnu make will look for files:
# VPATH = ./src

# check links and subdirectory modules before building AstroGK itself:
# (This order is important because some dependencies are known only
# by the Makefiles in the subdirectories, etc.).
all: $(LINKS) agk

agk: agk.o $(GKMOD) 
	$(FC) $(F90FLAGS) -o agk agk.o $(GKMOD) $(FLIBS)

expand: $(EXPMOD) expand.o 
	$(FC) $(F90FLAGS) -o expand expand.o $(EXPMOD) $(FLIBS)

test_exp: $(EXPMOD) test_exp.o 
	$(FC) $(F90FLAGS) -o test_exp test_exp.o $(EXPMOD) $(FLIBS)

#expand.o: expand.f90
#	xlf90 $(F90FLAGS) -c expand.f90 

file_utils.o: 
	$(FC) $(F90FLAGS) -c file_utils.f90

agk_dist_io.o: agk_dist_io.f90
	$(FC5) $(F90FLAGS) -c agk_dist_io.f90

ifeq ($(CPU),LINUX_lf95)

le_grids.o:
	$(FC) --quad $(F90FLAGS) -c le_grids.f90
endif

ifeq ($(CPU),Dawson)

le_grids.o:
	$(FC) $(F90FLAGS) -qautodbl=dbl -c le_grids.f90
endif

ifeq ($(CPU),RS6000)

le_grids.o: mp.o species.o theta_grid.o kgrids.o file_utils.o redistribute.o agk_layouts.o constants.o 
	$(FC) $(F90FLAGS) -qautodbl=dbl -c le_grids.f90
endif

#
# No optimizations for some routines: stupid workarounds because of 
# incompatibility between f95 and my C libraries (I think)
ifeq ($(CPU),LINUX)

agk_transforms.o: agk_transforms.f90
	$(FC) $(F90FLAGS_3) -c $<

nonlinear_terms.o: nonlinear_terms.f90
	$(FC) $(F90FLAGS_3) -c $<

netcdf_mod.o: netcdf_mod.f90
	$(FC) $(F90FLAGS_2) -c netcdf_mod.f90

endif
ifeq ($(CPU),LINUX_abs)

agk_io.o:
	$(FC) $(F90FLAGS_2) -c agk_io.f90
agk_save.o:
	$(FC) $(F90FLAGS_2) -c agk_save.f90
netcdf_mod.o:
	$(FC) $(F90FLAGS_2) -c netcdf_mod.f90

command_line.o:
	$(FC) $(F90FLAGS_1) -c command_line.f90
file_utils.o: 
	$(FC) $(F90FLAGS_1) -c file_utils.f90
mp.o:
	$(FC) $(F90FLAGS_1) -c mp.f90

fft_work.o:
	$(FC) $(F90FLAGS_0) -c fft_work.f90 
agk_transforms.o:  
	$(FC) $(F90FLAGS_0) -c agk_transforms.f90

endif

################################################################# DEPENDENCIES

file_utils.o: command_line.o mp.o file_utils.f90
antenna.o: species.o run_parameters.o file_utils.o mp.o agk_time.o antenna_data.o
antenna.o: kgrids.o theta_grid.o ran.o constants.o 
agk_layouts.o: mp.o file_utils.o
run_parameters.o: mp.o file_utils.o agk_save.o kgrids.o text_options.o 
run_parameters.o: agk_time.o
species.o: mp.o file_utils.o text_options.o
agk_save.o: theta_grid.o agk_layouts.o mp.o fields_arrays.o kgrids.o 
agk_save.o: file_utils.o antenna_data.o
agk_transforms.o: agk_layouts.o mp.o fft_work.o redistribute.o
agk_transform.o: theta_grid.o kgrids.o 
agk_diagnostics.o: file_utils.o kgrids.o run_parameters.o species.o mp.o 
agk_diagnostics.o: fields.o dist_fn.o constants.o agk_save.o agk_time.o
agk_diagnostics.o: agk_io.o le_grids.o fields_arrays.o dist_fn_arrays.o 
agk_diagnostics.o: agk_transforms.o nonlinear_terms.o collisions.o spl.o
agk_diagnostics.o: agk_heating.o agk_dist_io.o
agk_heating.o: mp.o species.o
dist_fn.o: mp.o species.o theta_grid.o kgrids.o le_grids.o antenna.o
dist_fn.o: run_parameters.o init_g.o text_options.o fft_work.o agk_heating.o
dist_fn.o: agk_layouts.o file_utils.o dist_fn_arrays.o constants.o  agk_time.o
dist_fn.o: collisions.o nonlinear_terms.o
dist_fn.o: agk_transforms.o agk_time.o redistribute.o hyper.o
hyper.o: kgrids.o run_parameters.o file_utils.o text_options.o 
hyper.o: mp.o agk_layouts.o theta_grid.o agk_time.o le_grids.o
init_g.o: mp.o species.o theta_grid.o kgrids.o le_grids.o dist_fn_arrays.o
init_g.o: agk_layouts.o agk_save.o fields_arrays.o ran.o text_options.o
init_g.o: file_utils.o run_parameters.o
le_grids.o: mp.o species.o theta_grid.o kgrids.o file_utils.o redistribute.o
le_grids.o: agk_layouts.o constants.o 
agk.o: mp.o file_utils.o fields.o run_parameters.o agk_diagnostics.o 
agk.o: agk_reinit.o agk_time.o init_g.o dist_fn_arrays.o
agk.o: agk_save.o 
fields.o: theta_grid.o kgrids.o run_parameters.o dist_fn.o mp.o
fields.o: file_utils.o dist_fn_arrays.o constants.o text_options.o
fields.o: fields_arrays.o fields_implicit.o
fields.o: init_g.o nonlinear_terms.o 
fields_implicit.o: theta_grid.o kgrids.o run_parameters.o dist_fn.o mp.o
fields_implicit.o: fields_arrays.o dist_fn_arrays.o antenna.o
fields_implicit.o: agk_layouts.o file_utils.o agk_save.o
fields_test.o: theta_grid.o kgrids.o run_parameters.o dist_fn.o mp.o
fields_test.o: fields_arrays.o dist_fn_arrays.o
fields_test.o: agk_layouts.o mp.o file_utils.o
kgrids.o: mp.o file_utils.o text_options.o constants.o 
theta_grid.o: mp.o file_utils.o text_options.o constants.o
collisions.o: mp.o species.o theta_grid.o kgrids.o le_grids.o agk_time.o
collisions.o: run_parameters.o file_utils.o text_options.o dist_fn_arrays.o
collisions.o: redistribute.o agk_layouts.o constants.o 
nonlinear_terms.o: theta_grid.o kgrids.o le_grids.o species.o agk_layouts.o 
nonlinear_terms.o: dist_fn_arrays.o agk_transforms.o run_parameters.o constants.o 
nonlinear_terms.o: text_options.o mp.o agk_time.o file_utils.o 
agk_reinit.o: collisions.o mp.o nonlinear_terms.o agk_time.o
agk_reinit.o: dist_fn.o fields.o fields_implicit.o
agk_reinit.o: init_g.o run_parameters.o agk_save.o dist_fn_arrays.o fields_arrays.o
agk_reinit.o: file_utils.o antenna.o  
redistribute.o: mp.o
agk_io.o: mp.o file_utils.o netcdf_mod.o kgrids.o theta_grid.o le_grids.o 
agk_io.o: species.o run_parameters.o fields_arrays.o 
agk_io.o: nonlinear_terms.o agk_layouts.o constants.o agk_transforms.o agk_heating.o
netcdf_mod.o: mp.o constants.o
ingen.o: file_utils.o text_options.o constants.o theta_grid.o
agk_dist_io.o: mp.o agk_transforms.o kgrids.o agk_layouts.o theta_grid.o
agk_dist_io.o: le_grids.o species.o file_utils.o

############################################################## MORE DIRECTIVES
clean:
	rm -f *.o *.mod *.g90 core */core ; \
	cd $(UTILS) ; rm -f *.o *.mod *.a ; cd .. ; \

distclean:	unlink clean
	rm -f agk

pack:	
	tar -cvf expand_src`date +'%y%m%d'`.tar Makefile *f90 test_os expand.in


############################################################### DEFAULT RULES

# If no other rules are found, use the defaults:

%.o : %.f90
	$(FC) $(F90FLAGS) -c $<

test_make:
	@echo FC is $(FC)
	@echo F90FLAGS is $(F90FLAGS)
	@echo FLIBS is $(FLIBS)
	@echo debug is $(debug)
	@echo CPU is $(CPU)

# If one of the platform-specific logical links doesn't exist, set them up:

$(LINKS):
	@$(MAKE) --no-print-directory $(PLATFORM_LINKS)

############################################################### PLATFORM LINKS
#
# Platform-specific logical links:
#

cxt4:
	ln -sf command_line_unix.f90 command_line.f90
	ln -sf mp_mpi_r8.f90 mp.f90
	ln -sf ran_portable.f90 ran.f90
	ln -sf file_utils_xlf.f90 file_utils.f90
	ln -sf fft_work_fftw.f90 fft_work.f90
	ln -sf agk_transforms_fftw.f90 agk_transforms.f90
	ln -sf agk_save_aix.f90 agk_save.f90
	ln -sf agk_dist_io_stub.f90 agk_dist_io.f90

ibm:
	ln -sf command_line_unix.f90 command_line.f90
	ln -sf mp_mpi_r8.f90 mp.f90
	ln -sf ran_portable.f90 ran.f90
	ln -sf file_utils_xlf.f90 file_utils.f90
	ln -sf fft_work_fftw.f90 fft_work.f90
	ln -sf agk_transforms_fftw.f90 agk_transforms.f90
	ln -sf agk_save_aix.f90 agk_save.f90
	ln -sf agk_dist_io_stub.f90 agk_dist_io.f90

linux:
	ln -sf command_line_nag.f90 command_line.f90
	ln -sf mp_mpi_r8.f90 mp.f90
	ln -sf ran_portable.f90 ran.f90
	ln -sf agk_save_fast.f90 agk_save.f90
	ln -sf file_utils_portable.f90 file_utils.f90
	ln -sf agk_transforms_fftw.f90 agk_transforms.f90
	ln -sf fft_work_fftw.f90 fft_work.f90
	ln -sf agk_dist_io_stub.f90 agk_dist_io.f90

linux_fuj:
	ln -sf command_line_unix.f90 command_line.f90
	ln -sf mp_stub.f90 mp.f90
	ln -sf ran_local.f90 ran.f90
	ln -sf file_utils_portable.f90 file_utils.f90
	ln -sf agk_save_fast.f90 agk_save.f90
	ln -sf agk_transforms_stub.f90 agk_transforms.f90
	ln -sf fft_work_stub.f90 fft_work.f90
	ln -sf agk_dist_io_stub.f90 agk_dist_io.f90

linux_lf95:
	ln -sf command_line_unix.f90 command_line.f90
	ln -sf mp_mpi_r8.f90 mp.f90
	ln -sf ran_portable.f90 ran.f90
	ln -sf file_utils_portable.f90 file_utils.f90
	ln -sf agk_save_fast.f90 agk_save.f90
	ln -sf agk_transforms_fftw.f90 agk_transforms.f90
	ln -sf fft_work_fftw.f90 fft_work.f90
	ln -sf agk_dist_io_stub.f90 agk_dist_io.f90

linux_abs:
	ln -sf command_line_unix.f90 command_line.f90
	ln -sf mp_mpi_r8.f90 mp.f90
	ln -sf ran_local.f90 ran.f90
	ln -sf agk_save_fast.f90 agk_save.f90
	ln -sf file_utils_portable.f90 file_utils.f90
	ln -sf agk_transforms_fftw.f90 agk_transforms.f90
	ln -sf fft_work_fftw.f90 fft_work.f90
	ln -sf agk_dist_io_stub.f90 agk_dist_io.f90

unlink:
	rm -f command_line.f90
	rm -f mp.f90
	rm -f ran.f90
	rm -f agk_save.f90
	rm -f agk_transforms.f90
	rm -f fft_work.f90
	rm -f file_utils.f90
	rm -f agk_dist_io.f90

