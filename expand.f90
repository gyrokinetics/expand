!
! Stand-alone code for expanding datasets. 
! Needs only to see 'netcdf.inc' for some definitions.
! Compile double precision.
!
program expand

!
! Note: This program assumes that the domain is triply periodic.
!
  use agk_layouts_serial, only: g_layout_type, gin_lo, gout_lo, init_dist_fn_layouts
  use agk_layouts_serial, only: ik_idx, it_idx, il_idx, ie_idx, is_idx
  use agk_layouts_serial, only: proc_id, idx, get_it_out, idx_g_proc
  
  implicit none
  
!
! nproc_in == # of files to read; use value of nproc from existing run
! nproc_out == # of files to write; use value of nproc for new run
!
  integer :: nproc_in, nproc_out
!
! layout_in == layout of files to be read
! layout_out == layout of files to be written
!
  character (len=5) :: layout_in, layout_out
!
! file_in == Base string of existing run
!
  character (300) :: file_in
!
! ntgrid_in == # of theta grid point to be read
! naky_in == # of ky modes to be read
! nakx_in == # of kx modes to be read
! nlambda_in == # of lambda grid points to be read
! negrid_in == # of energy grid points to be read
! nspec_in == # of species to be read
!
  integer :: ntgrid_in, naky_in, nakx_in, nlambda_in, negrid_in, nspec_in
!
! file_out == Base string of new run
!
  character (300) :: file_out

! ntgrid_out == # of theta grid point to be written
! naky_out == # of ky modes to be written
! nakx_out == # of kx modes to be written
! nlambda_out == # of lambda grid points to be written
! negrid_out == # of energy grid points to be written
! nspec_out == # of species to be written
!
  integer :: ntgrid_out, naky_out, nakx_out, nlambda_out, negrid_out, nspec_out

! tfac == Multiplicative factor for the timestep

  real :: tfac
! Files older than SVN rev 271 use NetCDF variables aperp_r and aperp_i
! If this flag is TRUE, it reads the value in as aperp but writes out as bpar
  logical :: old_aperp          !T=old file with NetCDF variable aperp
!
! use_Phi_in == Phi used in existing run?  logical
! use_Apar_in == Apar used in existing run?  logical
! use_Bpar_in == Bpar used in existing run?  logical
!
  logical :: use_Phi_in, use_Apar_in, use_Bpar_in
!
! use_Phi_out == Phi to be used in new run?  logical
! use_Apar_out == Apar to be used in new run?  logical
! use_Bpar_out == Bpar to be used in new run?  logical
!
  logical :: use_Phi_out, use_Apar_out, use_Bpar_out
!
! ant_on == antenna turned on?  logical
!
  logical :: ant_on
!
! Set write_h == T if you want to write
!
! h(z=0, ik_h, it_h, il_h, isgn=1) for each species.
! 
  logical :: write_h = .false.
!
! Local variables to hold Bessel function info when 
! write_h = .true.
! 
! Note: These are not just the Bessel functions; they also include the
! multipliers for each species.
!
  real, dimension(:), allocatable :: besj0, besj1
  real, dimension(:,:), allocatable :: energy
!
! it_h == index of kx grid for which h(E) will be written.
! ik_h == index of ky grid for which h(E) will be written.
! il_h == index of lambda grid for which h(E) will be written.
! ig_h == index of z grid for which h(E) will be written.
! 
  integer :: it_h, ik_h, il_h, ig_h
!
! Set write_inputs == T if you want to write new input files.
!
  logical :: write_inputs = .true.
!
! filename
!
  character (300) :: filename
!
! Non-inputs:
! t0 == sim time at end of existing run
! delt0 == sim delt at end of existing run
! nk_stir == # of antennae in existing run
! a_ant == amplitudes of antennae
! b_ant == amplitudes of antennae
!
! NetCDF stuff
!
  integer :: ncid, thetaid, signid, gloid, kyid, kxid, nk_stir_id
  integer :: phir_id, phii_id, aparr_id, apari_id, bparr_id, bpari_id
  integer :: delt0id, t0id, gr_id, gi_id
  integer :: a_antr_id, b_antr_id, a_anti_id, b_anti_id
  include 'netcdf.inc'

  integer :: nk_stir, iproc
  real :: t0, delt0
  complex, dimension(:), allocatable :: a_ant, b_ant
  complex, dimension(:,:,:), allocatable :: phi_in, apar_in, bpar_in
  complex, dimension(:,:,:), allocatable :: phi_out, apar_out, bpar_out
  real, dimension(:,:,:), allocatable :: gin_r, gin_i 
  real, dimension(:), allocatable :: h_r, h_i

  real, dimension(:), allocatable :: theta_in, theta_out
  real, dimension (:,:), allocatable :: subset_out

  integer :: ik_in, it_in, il_in, ie_in, is_in
  integer :: ik_out, it_out, il_out, ie_out, is_out
  integer :: ip_out, ip_last, n_elements, i

  character (306) :: file_proc  
  logical :: skip

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!            BEGINNING OF MAIN PROGRAM BLOCK           !!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  write(*,*) 'Reading in Parameters'
  call read_parameters                         ! Read input namelist
  call check_for_input_errors                  ! Report problems in input file
  call report                                  ! Report expected actions to screen

  call init_layouts                            ! Initialize data layout structures
  write(*,*) 'Reading Common Data'
  call get_common_data                         ! Get info common to all old datasets

  if (write_h) call write_hE                   ! Write h(E) if desired.

  if (write_inputs) then
     write(*,*)'Initializing Files'
     call init_newfiles                           ! Initialize new NetCDF files

     write(*,*) 'Writing Files'
     call write_newfiles                          ! Write old data into new files
  end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!                END OF MAIN PROGRAM BLOCK             !!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine read_parameters

    namelist /expand/ nproc_in, nproc_out, layout_in, layout_out, file_in, file_out, &
         ntgrid_in,  nakx_in,  naky_in,  nlambda_in,  negrid_in,  nspec_in, &
         ntgrid_out, nakx_out, naky_out, nlambda_out, negrid_out, nspec_out, tfac, &
         old_aperp, use_Phi_in,  use_Apar_in,  use_Bpar_in,  &
         use_Phi_out, use_Apar_out, use_Bpar_out, ant_on, &
         ik_h, it_h, il_h, ig_h, write_h, filename

    !Defaults
    old_aperp = .false.

    open (10, file="expand.in")
    read (10, nml=expand)                        ! No defaults provided
    close (10)

    call define_theta_grids                 ! Needed for later interpolations in theta

  end subroutine read_parameters

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine check_for_input_errors

    ! Error checking:
    if (file_in == file_out) then
       write (*,*) 'file_in and file_out should be different'
       stop
    end if
    
    if (negrid_in /= negrid_out) then
       write (*,*) 'negrid_in and negrid_out should be the same'
       stop
    end if
    
    if (nlambda_in /= nlambda_out) then
       write (*,*) 'nlambda_in and nlambda_out should be the same'
       stop
    end if
    
    if (nspec_in /= nspec_out) then
       write (*,*) 'nspec_in and nspec_out should be the same'
       stop 
    end if
    
    if (nakx_out < nakx_in) then
       write (*,*) 'nakx_out should not be less than nakx_in'
       stop
    end if
    
    if (naky_out < naky_in) then
       write (*,*) 'naky_out should not be less than naky_in'
       stop
    end if
  end subroutine check_for_input_errors

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine report

! Report expected actions to screen:

    write (*,*) 
    write (*,*) '------------------------------------------------------'
    write (*,*) 'Reading in data from ',trim(file_in)//'.00000, etc.'
    write (*,*) 'Writing data to ',trim(file_out)//'.00000, etc.'
    write (*,*)
    write (*,*) 'Expecting to read ',nproc_in,' files'

    if (write_inputs) then
       write (*,*)
       write (*,*) 'Writing data to ',trim(file_out)//'.0000, etc.'
       write (*,*) 'Expecting to write ',nproc_out,' files'
    end if
    
    if (write_h) then
       write (*,*)
       write (*,*) 'Expecting to write h(E) for '
       write (*,*) 'ig = ',ig_h
       write (*,*) 'ik = ',ik_h
       write (*,*) 'it = ',it_h
       write (*,*) 'il = ',il_h
    end if

    write (*,*) '------------------------------------------------------'
    write (*,*) 
    
  end subroutine report

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_layouts

    allocate ( gin_lo(0:nproc_in -1))             ! Allocate memory for old layout arrays
    allocate (gout_lo(0:nproc_out-1))             ! Allocate memory for new layout arrays
    
    call init_dist_fn_layouts (ntgrid_in, naky_in, nakx_in, & ! Initialize old layouts
         nlambda_in, negrid_in, nspec_in, nproc_in, layout_in, gin_lo(0:))   
    
    call init_dist_fn_layouts (ntgrid_out, naky_out, &        ! Initialize new layouts 
         nakx_out, nlambda_out, negrid_out, &
         nspec_out, nproc_out, layout_out, gout_lo(0:))         

  end subroutine init_layouts
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine get_common_data

    integer :: ip

    ip = 0
    call get_filename (file_in, ip)              ! Set file_proc = name of 0000 file
    call get_fields (ip)                         ! Get common data from 0000 file
    i = nf_close (ncid)                          ! Close the 0000 file     

  end subroutine get_common_data

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine get_filename (file, ip)
    character(300), intent (in) :: file
    integer, intent (in) :: ip
    integer :: th, h, t, u
    character (6) :: suffix
        
!    th = ip / 1000
!    h = (ip - th * 1000) / 100
!    t = (ip - th * 1000 - h * 100) / 10
!    u = (ip - th * 1000 - h * 100 - t * 10)
!    suffix = '.'//achar(48+th)//achar(48+h)//achar(48+t)//achar(48+u)
    write(suffix,'(a1,i5.5)')'.',ip
    !ERROR: file_proc is global and seems to bee getting confused with private
    ! subroutine variables
    file_proc = trim(trim(file)//suffix)

  end subroutine get_filename

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine get_fields (ip)

    use constants, only: zi
    integer, intent (in) :: ip
    double precision, allocatable, dimension(:,:,:) :: ftmpr, ftmpi
    double precision, allocatable, dimension(:) :: atmp
    double precision :: tmp1
    integer :: i, j

    i = nf_open (file_proc, nf_write, ncid)  
    write(*,*)'Opening file: ',file_proc
    !DEBUG
    if (.false.) write(*,*)'Getting fields:   ncid= ',ncid

    i = nf_inq_dimid (ncid, "theta", thetaid)
    i = nf_inq_dimid (ncid, "sign", signid)
    i = nf_inq_dimid (ncid, "glo", gloid)
    i = nf_inq_dimid (ncid, "aky", kyid)
    i = nf_inq_dimid (ncid, "akx", kxid)

!    i = nf_inq_dimlen (ncid, kyid, j)       
!    i = nf_inq_dimlen (ncid, kxid, j)       
!    i = nf_inq_dimlen (ncid, signid, j)       
    i = nf_inq_dimlen (ncid, gloid, j)       

    if (j /= gin_lo(ip)%ulim_proc-gin_lo(ip)%llim_proc+1) &
         write(*,*) 'Expand error: glo=? ',j,' : ',ip

    i= nf_inq_dimlen (ncid, thetaid, j)       
    if (j /= 2*ntgrid_in+1) &
         write(*,*) 'Expand error: ntgrid=? ',j,' : ',ntgrid_in,' : ',ip
    
! Read field information, t0, delt0, antennae stuff

    allocate (ftmpr(2*ntgrid_in+1,nakx_in,naky_in))
    allocate (ftmpi(2*ntgrid_in+1,nakx_in,naky_in))

    if (use_Phi_out) then
       i = nf_inq_varid (ncid, "phi_r", phir_id)
       i = nf_inq_varid (ncid, "phi_i", phii_id)
       i = nf_get_var_double (ncid, phir_id, ftmpr)
       i = nf_get_var_double (ncid, phii_id, ftmpi)

       allocate (phi_in (-ntgrid_in:ntgrid_in, nakx_in, naky_in))
       phi_in = cmplx(ftmpr, ftmpi)
    end if
    
    if (use_Apar_out) then
       i = nf_inq_varid (ncid, "apar_r", aparr_id)
       i = nf_inq_varid (ncid, "apar_i", apari_id)
       i = nf_get_var_double (ncid, aparr_id, ftmpr)
       i = nf_get_var_double (ncid, apari_id, ftmpi)

       allocate (apar_in (-ntgrid_in:ntgrid_in, nakx_in, naky_in))
       apar_in = cmplx(ftmpr, ftmpi)
    end if
    
    if (use_Bpar_out) then
       if (old_aperp) then
          i = nf_inq_varid (ncid, "aperp_r", bparr_id)
          i = nf_inq_varid (ncid, "aperp_i", bpari_id)
       else
          i = nf_inq_varid (ncid, "bpar_r", bparr_id)
          i = nf_inq_varid (ncid, "bpar_i", bpari_id)
       endif
       i = nf_get_var_double (ncid, bparr_id, ftmpr)
       i = nf_get_var_double (ncid, bpari_id, ftmpi)

       allocate (bpar_in (-ntgrid_in:ntgrid_in, nakx_in, naky_in))
       bpar_in = cmplx(ftmpr, ftmpi)
    end if
    
    deallocate (ftmpr, ftmpi)

! Get dt from end of run

    i = nf_inq_varid (ncid, "delt0", delt0id)
    i = nf_get_var_double (ncid, delt0id, tmp1)
    delt0 = tmp1

! Get time from end of run

    i = nf_inq_varid (ncid, "t0", t0id)
    i = nf_get_var_double (ncid, t0id, tmp1)
    t0 = tmp1

! Get antenna info from end of run

    if (ant_on) then

       i = nf_inq_dimid (ncid, "nk_stir", nk_stir_id)
       i = nf_inq_dimlen (ncid, nk_stir_id, j)
       nk_stir = j

       allocate (a_ant(nk_stir))  ; a_ant = 0.
       allocate (b_ant(nk_stir))  ; b_ant = 0.

       i = nf_inq_varid (ncid, "a_ant_r", a_antr_id)    
       i = nf_inq_varid (ncid, "a_ant_i", a_anti_id)
       i = nf_inq_varid (ncid, "b_ant_r", b_antr_id)
       i = nf_inq_varid (ncid, "b_ant_i", b_anti_id)
       
       allocate (atmp(nk_stir))  ;  atmp = 0.
       
       i = nf_get_var_double (ncid, a_antr_id, atmp)
       a_ant = atmp
       
       i = nf_get_var_double (ncid, a_anti_id, atmp)
       a_ant = a_ant + zi * atmp
       
       i = nf_get_var_double (ncid, b_antr_id, atmp)
       b_ant = atmp
       
       i = nf_get_var_double (ncid, b_anti_id, atmp)
       b_ant = b_ant + zi * atmp
       
       deallocate (atmp)

    end if

  end subroutine get_fields

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_newfiles

    double precision :: tmp1
    integer :: i, ip
    
    do ip = 0, nproc_out-1                             ! Loop over all new files
       
       n_elements = gout_lo(ip)%ulim_proc-gout_lo(ip)%llim_proc+1
       if (.false.) write(*,*)'ulim= ',gout_lo(ip)%ulim_proc, &
            ' llim= ',gout_lo(ip)%llim_proc,' nelements= ',n_elements
       !NOTE: If expanding to a large number of files (>1000), can this 
       !      prevent the last few files from being written GGH 20SEP07
       !NOTE: This cycle statement prevented necessary files from being created
       !      when nproc is not a factor of ulim_world---Just print warning GGH 29NOV07
!       if (n_elements <= 0) cycle
        if (n_elements <= 0) write(*,*)'WARNING: n_elements= ',n_elements


       call get_filename (file_out, ip)             ! Set file_proc = name of ipth file
       i = nf_create (file_proc, 0, ncid)           ! Create NetCDF file
       !DEBUG
       if (.false.) write(*,*)'Creating ',trim(file_proc),'     ncid = ', ncid
       call definitions                             ! Define variables, dimensions, etc.
       call write_zeroes(ip)                        ! Write zeroes for all of g, phi, Apar, Bpar
       
       tmp1 = t0                                 
       i = nf_put_var_double (ncid, t0id, tmp1)     ! Write value of t0        

       !Scale timestep by tfac to diminish timestep
       tmp1 = delt0*tfac                              
       i = nf_put_var_double (ncid, delt0id, tmp1)  ! Write value of delt0
       
       write(*,*)'Writing fields for processor ',ip
       call write_fields                            ! Write fields from existing run
       if (ant_on) call write_antenna               ! If antenna is on, write antenna data       
       i = nf_close (ncid)                          ! Close the file     
    end do
    
    !Deallocate fields data
    deallocate(phi_in,apar_in,bpar_in,phi_out,apar_out,bpar_out)
    deallocate(a_ant,b_ant)

  end subroutine init_newfiles

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine definitions

    integer :: i

    i = nf_def_dim (ncid, "theta", 2*ntgrid_out+1, thetaid)
    i = nf_def_dim (ncid, "sign", 2, signid)
    i = nf_def_dim (ncid, "glo", n_elements, gloid)
    i = nf_def_dim (ncid, "aky", naky_out, kyid)
    i = nf_def_dim (ncid, "akx", nakx_out, kxid)
    
    i = nf_def_var (ncid, "t0", NF_DOUBLE, 0, 0, t0id)
    i = nf_def_var (ncid, "delt0", NF_DOUBLE, 0, 0, delt0id)
    
    if (ant_on) then
       i = nf_def_dim (ncid, "nk_stir", nk_stir, nk_stir_id)
       i = nf_def_var (ncid, "a_ant_r", NF_DOUBLE, 1, nk_stir_id, a_antr_id)
       i = nf_def_var (ncid, "a_ant_i", NF_DOUBLE, 1, nk_stir_id, a_anti_id)
       i = nf_def_var (ncid, "b_ant_r", NF_DOUBLE, 1, nk_stir_id, b_antr_id)
       i = nf_def_var (ncid, "b_ant_i", NF_DOUBLE, 1, nk_stir_id, b_anti_id)
    end if

    i = nf_def_var (ncid, "gr", NF_DOUBLE, 3, (/ thetaid, signid, gloid /), gr_id)
    i = nf_def_var (ncid, "gi", NF_DOUBLE, 3, (/ thetaid, signid, gloid /), gi_id)
    
    if (use_Phi_out) then
       i = nf_def_var (ncid, "phi_r", NF_DOUBLE, 3, (/ thetaid, kxid, kyid /), phir_id)
       i = nf_def_var (ncid, "phi_i", NF_DOUBLE, 3, (/ thetaid, kxid, kyid /), phii_id)
    end if
    
    if (use_Apar_out) then
       i = nf_def_var (ncid, "apar_r", NF_DOUBLE, 3, (/ thetaid, kxid, kyid /), aparr_id)
       i = nf_def_var (ncid, "apar_i", NF_DOUBLE, 3, (/ thetaid, kxid, kyid /), apari_id)
    end if
    
    if (use_Bpar_out) then
       i = nf_def_var (ncid, "bpar_r", NF_DOUBLE, 3, (/ thetaid, kxid, kyid /), bparr_id)
       i = nf_def_var (ncid, "bpar_i", NF_DOUBLE, 3, (/ thetaid, kxid, kyid /), bpari_id)
    end if
    
    i = nf_enddef (ncid)

  end subroutine definitions

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine write_zeroes (ip)  ! Initialize data with zeroes everywhere

    integer, intent (in) :: ip
    double precision, allocatable, dimension(:,:,:) :: tmpr
    integer :: i, lo, hi

    lo = gout_lo(ip)%llim_proc
    hi = gout_lo(ip)%ulim_alloc

    allocate (tmpr(2*ntgrid_out+1,2,lo:hi))  ;  tmpr = 0.

    i = nf_put_var_double (ncid, gr_id, tmpr)
    i = nf_put_var_double (ncid, gi_id, tmpr)
    
    deallocate (tmpr)
    
    allocate (tmpr(2*ntgrid_out+1,nakx_out,naky_out))    ;  tmpr = 0.

    if (use_Phi_out) then
       i = nf_put_var_double (ncid, phir_id, tmpr)
       i = nf_put_var_double (ncid, phii_id, tmpr)
    end if
    
    if (use_Apar_out) then
       i = nf_put_var_double (ncid, aparr_id, tmpr)
       i = nf_put_var_double (ncid, apari_id, tmpr)
    end if
    
    if (use_Bpar_out) then
       i = nf_put_var_double (ncid, bparr_id, tmpr)
       i = nf_put_var_double (ncid, bpari_id, tmpr)
    end if
    
    deallocate (tmpr)
    
  end subroutine write_zeroes

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine write_fields

    double precision, allocatable, dimension(:,:,:) :: ftmp
    integer :: i
    logical, save :: first = .true.

    if (first) then

       if (use_Phi_out) then
          allocate (phi_out (-ntgrid_out:ntgrid_out, nakx_out, naky_out))
          call interpolate_field (phi_in, phi_out)   ! Interpolate & reindex
!TEST          phi_out=phi_in        
       end if

       if (use_Apar_out) then
          allocate (apar_out (-ntgrid_out:ntgrid_out, nakx_out, naky_out))
          call interpolate_field (apar_in, apar_out) ! Interpolate & reindex
!TEST          apar_out=apar_in
       end if

       if (use_Bpar_out) then
          allocate (bpar_out (-ntgrid_out:ntgrid_out, nakx_out, naky_out))
          call interpolate_field (bpar_in, bpar_out) ! Interpolate & reindex
!TEST          bpar_out=bpar_in
       end if

          !TEST
          if (.false.) then
             write(*,*)'phi_in'
             do i=1,nakx_in
                write(*,'(10es10.2)')real(phi_in(0,i,:))
             enddo
             write(*,*)'phi_out'
             do i=1,nakx_out
                write(*,'(10es10.2)')real(phi_out(0,i,:))
             enddo
             write(*,*)'phi_in'
             do i=1,nakx_in
                write(*,'(10es10.2)')aimag(phi_in(0,i,:))
             enddo
             write(*,*)'phi_out'
             do i=1,nakx_out
                write(*,'(10es10.2)')aimag(phi_out(0,i,:))
             enddo
             write(*,*)'apar_in'
             do i=1,nakx_in
                write(*,'(10es10.2)')real(apar_in(0,i,:))
             enddo
             write(*,*)'apar_out'
             do i=1,nakx_out
                write(*,'(10es10.2)')real(apar_out(0,i,:))
             enddo
             write(*,*)'apar_in'
             do i=1,nakx_in
                write(*,'(10es10.2)')aimag(apar_in(0,i,:))
             enddo
             write(*,*)'apar_out'
             do i=1,nakx_out
                write(*,'(10es10.2)')aimag(apar_out(0,i,:))
             enddo
             write(*,*)'bpar_in'
             do i=1,nakx_in
                write(*,'(10es10.2)')real(bpar_in(0,i,:))
             enddo
             write(*,*)'bpar_out'
             do i=1,nakx_out
                write(*,'(10es10.2)')real(bpar_out(0,i,:))
             enddo
             write(*,*)'bpar_in'
             do i=1,nakx_in
                write(*,'(10es10.2)')aimag(bpar_in(0,i,:))
             enddo
             write(*,*)'bpar_out'
             do i=1,nakx_out
                write(*,'(10es10.2)')aimag(bpar_out(0,i,:))
             enddo
          endif

       first = .false.
    endif

    allocate (ftmp(2*ntgrid_out+1,nakx_out,naky_out))

    if (use_Phi_out) then
       ftmp = real(phi_out)
       i = nf_put_var_double (ncid, phir_id, ftmp)

       ftmp = aimag(phi_out)
       i = nf_put_var_double (ncid, phii_id, ftmp)
    end if

    if (use_Apar_out) then
       ftmp = real(apar_out)
       i = nf_put_var_double (ncid, aparr_id, ftmp)

       ftmp = aimag(apar_out)
       i = nf_put_var_double (ncid, apari_id, ftmp)
    end if

    if (use_Bpar_out) then
       ftmp = real(bpar_out)
       i = nf_put_var_double (ncid, bparr_id, ftmp)

       ftmp = aimag(bpar_out)
       i = nf_put_var_double (ncid, bpari_id, ftmp)
    end if

    deallocate(ftmp)

  end subroutine write_fields

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine write_antenna

    double precision, allocatable, dimension(:) :: atmp
    integer :: i

    allocate (atmp(nk_stir))

    atmp = real(a_ant)
    i = nf_put_var_double (ncid, a_antr_id, atmp)
    
    atmp = aimag(a_ant)
    i = nf_put_var_double (ncid, a_anti_id, atmp)
    
    atmp = real(b_ant)
    i = nf_put_var_double (ncid, b_antr_id, atmp)
    
    atmp = aimag(b_ant)
    i = nf_put_var_double (ncid, b_anti_id, atmp)
    
    deallocate (atmp)

  end subroutine write_antenna

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine write_newfiles

    double precision, dimension(:,:,:), allocatable :: tmp
    integer :: ipfrom, ip_last, i, iglo_out, iglo, k

    allocate(subset_out(-ntgrid_out:ntgrid_out, 2)) ! Allocate array for a single iglo for output 
    allocate(tmp(-ntgrid_out:ntgrid_out, 2, 1))     ! Allocate double prec array for NetCDF stupidity
    
    ip_last=0
    do ipfrom = 0, nproc_in-1                  ! Loop over old datasets


       write(*,*)'Reading distribution function for processor ',ipfrom
       call open_nc_read (file_in, ipfrom, ncid) ! Open old dataset
       call get_g (ipfrom)                   ! Get g from this dataset; store in gin_r, gin_i
       i = nf_close (ncid)                  ! Close the old data file from processor # ipfrom
       if (skip) cycle                      ! If no data in this file, cycle out of loop

!
! Loop over iglo indices in this dataset and write them to the appropriate file.
!
! Since consecutive values of iglo_in will often go to the same file, start each 
! trip through the loop with a file open.  Guess that the first file is the file
! associated with the processor that the data came from.  (Just a guess.)
!
       !NOTE: This crashes if you want to reduce number of processors
!       ip_last = ipfrom
       ! REPLACED WITH
!       ip_last = max(ipfrom,nproc_out-1)  !UNNECESSARY
       write(*,'(a,i6)')'Opening file ',ip_out
       call open_nc_write (file_out, ip_last, ncid)  

       !DEBUG
       if (.false.) write(*,*)'From proc ',ipfrom,' working on iglos: ',gin_lo(ipfrom)%llim_proc,' to ',&
            gin_lo(ipfrom)%ulim_proc

       do iglo = gin_lo(ipfrom)%llim_proc, gin_lo(ipfrom)%ulim_proc

          ik_in = ik_idx (gin_lo(ipfrom),iglo) ! Get ky index for this iglo
          it_in = it_idx (gin_lo(ipfrom),iglo) ! Get kx index for this iglo
          il_in = il_idx (gin_lo(ipfrom),iglo) ! Get lambda index 
          ie_in = ie_idx (gin_lo(ipfrom),iglo) ! Get energy index
          is_in = is_idx (gin_lo(ipfrom),iglo) ! Get species index
        
! NOTE: it and ik dimension may change, so we must map these correctly
! Generate output iglo from output gout_lo
! il, ie, is, and ik are directly copied

          il_out = il_in
          ie_out = ie_in
          ik_out = ik_in
          is_out = is_in

! For it, we have to account for the wrapping of negative modes

          it_out = get_it_out(it_in, nakx_in, nakx_out)

! Generate new global target index for this datapoint
! NOTE: ipfrom is a dummy variable in this call:
! ERR: replaced ipfrom with 0 since it is a dummy anyway (crashed from reducing number of files) GGH 15JAN08
!          iglo_out = idx (gout_lo(ipfrom), ik_out, it_out, il_out, ie_out, is_out)
          iglo_out = idx (gout_lo(0), ik_out, it_out, il_out, ie_out, is_out)

! Find target proc for this target iglo
! NOTE: ipfrom is a dummy variable in this call:
!        
! ERR: replaced ipfrom with 0 since it is a dummy anyway (crashed from reducing number of files) GGH 15JAN08
!          ip_out = proc_id (gout_lo(ipfrom), iglo_out)
          ip_out = proc_id (gout_lo(0), iglo_out)

          !TEST
          if (.false. .and. mod(iglo,100) .eq. 0) then
             write(*,*)'il_in= ',il_in,' il_out= ',il_out
             write(*,*)'ie_in= ',ie_in,' ie_out= ',ie_out
             write(*,*)'is_in= ',is_in,' is_out= ',is_out
             write(*,*)'ik_in= ',ik_in,' ik_out= ',ik_out
             write(*,*)'it_in= ',it_in,' it_out= ',it_out
             write(*,*)'ip_in= ',ipfrom,' ip_out= ',ip_out
             write(*,*)'iglo_in= ',iglo,' iglo_out= ',iglo_out
          endif


! Get the correct output file open

          if (ip_out .ne. ip_last) then
             write(*,'(a,i6,a,i6)')'Closing file ',ip_last,' and opening file ',ip_out
             i = nf_close (ncid)                    ! Close the open datafile
             call open_nc_write (file_out, ip_out, ncid)  ! Open the correct datafile
             ip_last = ip_out                       ! Save number of open datafile
          end if

! Interpolate onto the new theta grid and write it

       call interpolate (gin_r(:,:,iglo), subset_out(:,:)) ! interpolate gin_r to subset_out
!TEST          subset_out(:,:)=gin_r(:,:,iglo)
          tmp(:,:,1) = subset_out(:,:)              ! Writes ubset_out to file (real part)
          
          k = idx_g_proc (gout_lo(0), iglo_out)

         !TEST
          if (.false.) then
             write(*,*)'(',ik_out,',',it_out,',',il_out,',',ie_out,',',is_out,'): ', &
                  iglo,ipfrom,'=>',iglo_out,ip_out, subset_out(0,1)
          endif

          i = nf_put_vara_double (ncid, gr_id, &          
               (/ 1, 1, k /), &
               (/ 2*ntgrid_out + 1, 2, 1 /), tmp)

          call interpolate (gin_i(:,:,iglo), subset_out(:,:)) ! interpolate gin_i to subset_out
!TEST          subset_out(:,:)=gin_i(:,:,iglo)
          tmp(:,:,1) = subset_out(:,:)              ! Write subset_out to file (imag part)
          i = nf_put_vara_double (ncid, gi_id, &          
               (/ 1, 1, k /), &
               (/ 2*ntgrid_out + 1, 2, 1 /), tmp)
          
       end do
       write(*,'(a,i6)')'Closing file ',ip_out
       i= nf_close(ncid)                      ! Close the open datafile
       call clean_up_g                        ! Different size gin arrays could be lurking
    end do
    
    deallocate (subset_out)
    deallocate (tmp)
    
  end subroutine write_newfiles
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine get_g (ip)

    integer, intent(in) :: ip
    double precision, allocatable, dimension(:,:,:) :: tmpr, tmpi
    integer :: i, lo, hi

    if (n_elements > 0) then
       lo = gin_lo(ip)%llim_proc
       hi = gin_lo(ip)%ulim_proc              ! n_elements > 0, so no need to use _alloc

       allocate (tmpr(2*ntgrid_in+1,2,lo:hi))
       allocate (tmpi(2*ntgrid_in+1,2,lo:hi))
       
       allocate (gin_r(-ntgrid_in:ntgrid_in,2,lo:hi))
       allocate (gin_i(-ntgrid_in:ntgrid_in,2,lo:hi))

       i = nf_get_var_double (ncid, gr_id, tmpr)
       i = nf_get_var_double (ncid, gi_id, tmpi)

       gin_r = tmpr
       gin_i = tmpi
       
       deallocate (tmpr, tmpi)

       skip = .false.

    else

       skip = .true.

    end if

  end subroutine get_g

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine clean_up_g

    if (allocated(gin_r)) deallocate (gin_r, gin_i)

  end subroutine clean_up_g

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine write_hE 

    real :: b0, b1, e
    integer :: ipfrom, i, iglo, iglo_in, is

    open (89, file=trim(filename)//'.bes')

    allocate (energy(negrid_in, nspec_in)) ; energy = 0.
    allocate (besj0(gin_lo(0)%llim_world:gin_lo(0)%ulim_world))  ; besj0 = 0.
    allocate (besj1(gin_lo(0)%llim_world:gin_lo(0)%ulim_world))  ; besj1 = 0.
    allocate (h_r(gin_lo(0)%llim_world:gin_lo(0)%ulim_world)) ;  h_r = 0.
    allocate (h_i(gin_lo(0)%llim_world:gin_lo(0)%ulim_world)) ;  h_i = 0.

    do ipfrom = 0, nproc_in-1                  ! Loop over old datasets

       write(*,*) 'Reading distribution function for processor ',ipfrom
       call open_nc_read (file_in, ipfrom, ncid) ! Open old dataset
       call get_g (ipfrom)                       ! Get g from this dataset; store in gin_r, gin_i
       i = nf_close (ncid)                       ! Close the old data file from processor # ipfrom
       if (skip) cycle                           ! If no data in this file, cycle out of loop

!
! Loop over iglo indices in this dataset and save them as appropriate.
!

       do iglo = gin_lo(ipfrom)%llim_proc, gin_lo(ipfrom)%ulim_proc

          ik_in = ik_idx (gin_lo(ipfrom),iglo) ! Get ky index for this iglo
          it_in = it_idx (gin_lo(ipfrom),iglo) ! Get kx index for this iglo
          il_in = il_idx (gin_lo(ipfrom),iglo) ! Get lambda index 
          ie_in = ie_idx (gin_lo(ipfrom),iglo) ! Get energy index
          is_in = is_idx (gin_lo(ipfrom),iglo) ! Get species index

          read (89, *) iglo_in, b0, b1, e

          energy(ie_in, is_in) = e   ! overkill, fix it later

          if (iglo_in == iglo) then
             besj0(iglo) = b0
             besj1(iglo) = b1
          else
             write (*,*) '*******************************************'
             write (*,*) ' Error reading Bessel function information ',iglo_in
             write (*,*) '*******************************************' 
          end if
             
!!
!! At this point, we have g, Phi and Bpar for each species, as a function of theta and isgn.
!! Need to form h(E, ig=ig_h, isgn=1) and save it.
!!
          if (use_phi_in) then          
             h_r(iglo) = gin_r(ig_h,1,iglo)+besj0(iglo)* real(phi_in(ig_h,it_in,ik_in)) 
             h_i(iglo) = gin_i(ig_h,1,iglo)+besj0(iglo)*aimag(phi_in(ig_h,it_in,ik_in))
          else
             h_r(iglo) = gin_r(ig_h,1,iglo)
             h_i(iglo) = gin_i(ig_h,1,iglo)
          end if

          if (use_bpar_in) then
             h_r(iglo) = h_r(iglo) + besj1(iglo)* real(bpar_in(ig_h,it_in,ik_in))
             h_i(iglo) = h_i(iglo) + besj1(iglo)*aimag(bpar_in(ig_h,it_in,ik_in))
          end if
          
       end do

       call clean_up_g                        ! Different size gin arrays could be lurking
    end do
    
    close (89)  ! Bessel function information
    

    open (89, file=trim(filename)//'.hE')
    
    do ipfrom = 0, nproc_in-1                  ! Loop over old datasets
       do iglo=gin_lo(ipfrom)%llim_proc, gin_lo(ipfrom)%ulim_proc

          il_in = il_idx(gin_lo(ipfrom), iglo)
          ik_in = ik_idx(gin_lo(ipfrom), iglo)
          it_in = it_idx(gin_lo(ipfrom), iglo)

          if (il_h /= il_in) cycle
          if (ik_h /= ik_in) cycle
          if (it_h /= it_in) cycle

          ie_in = ie_idx(gin_lo(ipfrom), iglo)
          is_in = is_idx(gin_lo(ipfrom), iglo)

          write (89, fmt="('E= ',e16.10,' Re[h(E)]= ',e10.4, &
               & ' Im[h(E)]= ',e10.4,' is= ',i4)") &
               & energy(ie_in,is_in), h_r(iglo), h_i(iglo), is_in
          
       end do
    end do
    

    
  end subroutine write_hE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine interpolate (f, h)

    use splines
    use constants, only: pi
    implicit none

    real, dimension (-ntgrid_in:,:) :: f
    real, dimension (-ntgrid_out:,:) :: h
    integer :: ig, isgn
    type (periodic_spline) :: spl

    do isgn=1,2

       call new_periodic_spline (2*ntgrid_in, theta_in, f(:,isgn), 2.*pi, spl)

       do ig=-ntgrid_out, ntgrid_out-1
          h(ig,isgn) = periodic_splint (theta_out(ig), spl)
       end do

       h(ntgrid_out, isgn) = h (-ntgrid_out, isgn)

       call delete_periodic_spline (spl)
    end do

    
  end subroutine interpolate

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine interpolate_field (f_in, f_out)

    use splines
    use constants, only: pi
    implicit none

    complex, dimension ( -ntgrid_in:,:,:) :: f_in
    complex, dimension (-ntgrid_out:,:,:) :: f_out
    real, dimension (:,:,:), allocatable :: fr_in, fr_out, fi_in, fi_out
    integer :: ik, it, it_out, ig

    type (periodic_spline) :: spl
    
    allocate (fr_in(-ntgrid_in:ntgrid_in, nakx_in, naky_in))
    allocate (fi_in(-ntgrid_in:ntgrid_in, nakx_in, naky_in))
    allocate (fr_out(-ntgrid_out:ntgrid_out, nakx_out, naky_out))
    allocate (fi_out(-ntgrid_out:ntgrid_out, nakx_out, naky_out))

    do ik = 1,naky_in
       do it = 1, nakx_in
          
          fr_in = real(f_in)
          fi_in = aimag(f_in)

          it_out = get_it_out (it, nakx_in, nakx_out)
          
          call new_periodic_spline (2*ntgrid_in, theta_in, fr_in(:,it,ik), 2.*pi, spl)
          do ig=-ntgrid_out, ntgrid_out-1
             fr_out(ig,it_out,ik) = periodic_splint (theta_out(ig), spl)
          end do
          fr_out (ntgrid_out, it_out, ik) = fr_out (-ntgrid_out, it_out, ik)
          call delete_periodic_spline (spl)

          call new_periodic_spline (2*ntgrid_in, theta_in, fi_in(:,it,ik), 2.*pi, spl)
          do ig=-ntgrid_out, ntgrid_out-1
             fi_out(ig,it_out,ik) = periodic_splint (theta_out(ig), spl)
          end do
          fi_out (ntgrid_out, it_out, ik) = fi_out (-ntgrid_out, it_out, ik)
          call delete_periodic_spline (spl)

       end do
    end do

    f_out = cmplx (fr_out, fi_out)

    deallocate(fr_in,fi_in,fr_out,fi_out)

  end subroutine interpolate_field

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine open_nc_read (file, ip_tmp, ncid)  

    character (300), intent (in) :: file
    integer, intent (in) :: ip_tmp
    integer :: ncid, i, j
    integer :: nakx_tmp,naky_tmp,nsign_tmp
!    character(80) :: 

    call get_filename (file, ip_tmp)          ! Set file_proc = name of ipth file
    i = nf_open (trim(file_proc), nf_nowrite, ncid)    ! Open NetCDF file; get ncid
    if (i .ne. nf_noerr) then
       write(*,*)'File: ',trim(file_proc),' Status: ',i
       write(*,*)'Error: ',nf_strerror(i)
    endif
    
    i = nf_inq_dimid (ncid, "theta", thetaid)  ! Get dims for reads ...
    i = nf_inq_dimid (ncid, "sign", signid)
    i = nf_inq_dimid (ncid, "glo", gloid)
    i = nf_inq_dimid (ncid, "aky", kyid)
    i = nf_inq_dimid (ncid, "akx", kxid)
    
    i = nf_inq_dimlen (ncid, signid, nsign_tmp)       
    i = nf_inq_dimlen (ncid, kyid, naky_tmp)       
    i = nf_inq_dimlen (ncid, kxid, nakx_tmp)       
    i = nf_inq_dimlen (ncid, gloid, n_elements)     
    
    i = nf_inq_varid (ncid, "gr", gr_id)
    i = nf_inq_varid (ncid, "gi", gi_id)

    !DEBUG
    if (.false.) then
       write(*,*)'Opening File: ',trim(file_proc)
       write(*,*)'ncid =        ',ncid
       write(*,*)'gloid=        ',gloid
       write(*,*)'nakx =        ',nakx_tmp
       write(*,*)'naky =        ',naky_tmp
       write(*,*)'nsign =       ',nsign_tmp
       write(*,*)'n_elements=   ',n_elements
    endif
  
  end subroutine open_nc_read
  subroutine open_nc_write (file, ip_tmp, ncid)  

    character (300), intent (in) :: file
    integer, intent (in) :: ip_tmp
    integer :: ncid, i, j
    integer :: nakx_tmp,naky_tmp,nsign_tmp
!    character(80) :: 

    call get_filename (file, ip_tmp)          ! Set file_proc = name of ipth file
    i = nf_open (trim(file_proc), nf_write, ncid)    ! Open NetCDF file; get ncid
    if (i .ne. nf_noerr) then
       write(*,*)'File: ',trim(file_proc),' Status: ',i
       write(*,*)'Error: ',nf_strerror(i)
    endif
    
    i = nf_inq_dimid (ncid, "theta", thetaid)  ! Get dims for reads ...
    i = nf_inq_dimid (ncid, "sign", signid)
    i = nf_inq_dimid (ncid, "glo", gloid)
    i = nf_inq_dimid (ncid, "aky", kyid)
    i = nf_inq_dimid (ncid, "akx", kxid)
    
    i = nf_inq_dimlen (ncid, signid, nsign_tmp)       
    i = nf_inq_dimlen (ncid, kyid, naky_tmp)       
    i = nf_inq_dimlen (ncid, kxid, nakx_tmp)       
    i = nf_inq_dimlen (ncid, gloid, n_elements)     
    
    i = nf_inq_varid (ncid, "gr", gr_id)
    i = nf_inq_varid (ncid, "gi", gi_id)

    !DEBUG
    if (.false.) then
       write(*,*)'Opening File: ',trim(file_proc)
       write(*,*)'ncid =        ',ncid
       write(*,*)'gloid=        ',gloid
       write(*,*)'nakx =        ',nakx_tmp
       write(*,*)'naky =        ',naky_tmp
       write(*,*)'nsign =       ',nsign_tmp
       write(*,*)'n_elements=   ',n_elements
    endif
  
  end subroutine open_nc_write

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine define_theta_grids
    use constants, only: pi
    integer :: ntheta_in, ntheta_out
!
! Assume nperiod = 1 for now.  (Will be a good assumption for AstroGK.)
!
    ntheta_in = 2*ntgrid_in
    allocate (theta_in(-ntgrid_in:ntgrid_in))
    theta_in = (/ (real(i)*2.0*pi/real(ntheta_in), i=-ntgrid_in,ntgrid_in) /)
    
    ntheta_out = 2*ntgrid_out
    allocate (theta_out(-ntgrid_out:ntgrid_out))
    theta_out = (/ (real(i)*2.0*pi/real(ntheta_out), i=-ntgrid_out,ntgrid_out) /)

  end subroutine define_theta_grids


end program expand
